import React, { useContext } from "react";

import { createStackNavigator } from '@react-navigation/stack';
import Home from "../home";
import Categori from "../categori";
import SubCategori from "../subcategori";
import Autre from "../autre";
import Commander from "../commander";
import CommandeAutre from "../commandeAutre";
import SubCategorie from "../subCategorie";
import VotreCommande from "../votreCommande";
import PasserCommande from "../passerCommand";
import Signin from "../signin";
import ComfirmedCommande from "../confirmedCommande";
import SiginUp from "../signup";
import Contacter from "../contacter";
import APropos from "../APropos";
import ResetPassword from "../resetPassowrd";
import MonCompte from "../MonCompte";

const Stack = createStackNavigator();
export function AutherNavigator({ props, navigation }) {
  return (

    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={({ route, navigation }) => ({
        gestureEnabled: false,
        cardOverlayEnabled: true,
         headerStatusBarHeight:
          navigation.dangerouslyGetState().routes.indexOf(route) === 0
            ? 0
            : undefined
      })}
      mode="modal"
    >
      <Stack.Screen name="Home" component={Home}
         options={{ header: () => null ,headerTitle:null}} 
        />
       
        <Stack.Screen  name="Settings" component={Categori} 
         options={{ header: () => null ,headerTitle:null}} 
        />
        <Stack.Screen name="SubCategori" component={SubCategori}
         options={{ header: () => null ,headerTitle:null}} 
        />
        <Stack.Screen name="Autre" component={Autre}
         options={{ header: () => null ,headerTitle:null}} 
        />

<Stack.Screen name="Commander" component={Commander}
         options={{ header: () => null ,headerTitle:null}} 
        />

<Stack.Screen name="CommandeAutre" component={CommandeAutre}
         options={{ header: () => null ,headerTitle:null}} 
        />
        
        <Stack.Screen name="SubCategorie" component={SubCategorie}
         options={{ header: () => null ,headerTitle:null}} 
        />
         <Stack.Screen name="VotreCommande" component={VotreCommande}
         options={{ header: () => null ,headerTitle:null}} 
        />
  <Stack.Screen name="PasserCommande" component={PasserCommande}
         options={{ header: () => null ,headerTitle:null}} 
        />
        
        <Stack.Screen name="Signin" component={Signin}
         options={{ header: () => null ,headerTitle:null}} 
        />
           <Stack.Screen name="ComfirmedCommande" component={ComfirmedCommande}
         options={{ header: () => null ,headerTitle:null}} 
        />
  <Stack.Screen name="SiginUp" component={SiginUp}
         options={{ header: () => null ,headerTitle:null}} 
        />
          <Stack.Screen name="Contacter" component={Contacter}
         options={{ header: () => null ,headerTitle:null}} 
        />
 <Stack.Screen name="APropos" component={APropos}
         options={{ header: () => null ,headerTitle:null}} 
        />
 <Stack.Screen name="ResetPassword" component={ResetPassword}
         options={{ header: () => null ,headerTitle:null}} 
        />
 <Stack.Screen name="MonCompte" component={MonCompte}
         options={{ header: () => null ,headerTitle:null}} 
        />

        
      
    </Stack.Navigator>
  
  )
}