import React, { useContext } from "react";



import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';



import { createDrawerNavigator } from '@react-navigation/drawer';
import  SideMenuScreen  from '../SideMenuScreen/index'
import { AutherNavigator } from "./SeriveNavigation";


const Drawer = createDrawerNavigator();

export function DrawerStack({props , navigation })  {
  
    return (
 
      <Drawer.Navigator 
       drawerContent={props => <SideMenuScreen {...props} /> }    drawerWidth={"360"}   >
  
           <Drawer.Screen name="AutherNavigator" component={AutherNavigator}  />
          
      
        </Drawer.Navigator>
     
    );
  }