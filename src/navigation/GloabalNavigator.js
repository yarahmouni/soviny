import React, { useContext } from "react";
import { createStackNavigator } from '@react-navigation/stack';
import { DrawerStack } from "./MainNavigator";
import Home from "../home";
import { AutherNavigator } from "./SeriveNavigation";

 const Stack = createStackNavigator();
export function GlobalStack ({props , navigation })  {
  
  return (
    <Stack.Navigator 
    initialRouteName="AutherNavigator"
     >
        
      <Stack.Screen 
       options={{ header: () => null ,headerTitle:null}} 
      name="AutherNavigator" component={AutherNavigator}    />
     
      
    </Stack.Navigator>
  );
}