import React, { useContext } from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { DrawerStack } from "./MainNavigator";
import Home from "../home";
import Categori from "../categori";
import Telephone from "../Telephone";

const Tab = createBottomTabNavigator();

export function TabNavigator ({props , navigation })  {
  
  return (
   
     <Tab.Navigator  initialRouteName="DrawerStack">
        <Tab.Screen name="DrawerStack" component={DrawerStack} />
        <Tab.Screen name="Telephone" component={Telephone} />
      </Tab.Navigator>
     
      
   
  );
}