import React, { Component } from 'react'
import { Text, View ,Image,TouchableOpacity,StyleSheet,TextInput} from 'react-native'
import Ionicons from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';
import {Register} from "../servicesApi/authentification/index"
import validate from '../passerCommand/validation';
export default class SiginUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
         email:"",
         password:"",
         username:"",
         customer_note:"",
         scurite:true
        };
      }
      componentDidMount(){
      
          if(this.props.route?.params?.customer_note){
            this.setState({
              customer_note:this.props.route?.params?.customer_note
            })
          }
        
 
      }
      login=async()=>{
        console.log("emaill===>",this.state.email)
        if(this.state.password===""){
          alert("Veuillez insérer mot de passe")
        }else{
  const res=  await   Register(this.state.email,this.state.email,this.state.password);
  console.log("res login",res)
if(res.data){
    if(res.data.status=="ok"){
        this.props.navigation.navigate("PasserCommande",{"email":this.state.email,"item":this.props.route.params.item,"customer_note":this.state.customer_note})
       
    }else{
      if(res.data.error==="E-mail address is already in use."){
        alert("Cette adresse email existe déjà")

      }else if(res.data.error==="Username already exists."){
        alert("Cette adresse email existe déjà")
      }
      console.log("uerrrrrr=>",res.data.error)
       
    }
}else{
    alert("verifier votre email et  mote de passe") 
}
        }

      }

    render() {
        return (
            <View style={{flex:1}}>
            <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={styles.linearGradient}>
            <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
              
           <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}} >
           <TouchableOpacity
           style={{height:60,width:70,justifyContent:"center"}}
           onPress={()=> this.props.navigation.goBack()}>
           <View style={{alignContent:"center",justifyContent:"center"}}>
            <Ionicons name={"chevron-left"} size={20}  color="white"/>
            </View>
            </TouchableOpacity>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}} >
              <Text style={{color:"white",fontSize:20}}>Je m'inscris</Text>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center", height:60,width:70}} >
         
            </View>
            </View>
            <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
            <View style={{marginHorizontal:30}}>
            <View style={{justifyContent:"center",height:50}}>
                <Text style={{fontSize:15,color:"black"}}>
                Je m'inscris
                </Text>
                </View>
            </View>
            <ScrollView style={{flex:1,marginHorizontal:20}}>
            {/* <View style={{  marginTop: 5 }}>
                  <Text>Nom d'utilisateur</Text>
                  <TextInput
                    placeholder="Nom d'utilisateur"
                    style={{
                      height: 35,
                      borderWidth: 0.5,
                      borderRadius: 5,
                      marginTop: 5,
                      paddingVertical: 1,
                    }}
                    onChangeText={(e)=>{this.setState({
                        username:e
                    })}}
                  ></TextInput>
                </View> */}
            <View style={{  marginTop: 5 }}>
                  <Text>Email</Text>
                  <TextInput
                    placeholder="Email"
                    style={{
                      height: 35,
                      borderWidth: 0.5,
                      borderRadius: 5,
                      marginTop: 5,
                      paddingVertical: 1,
                    }}
                    onChangeText={(e)=>{this.setState({
                        email:e
                    })}}
                  ></TextInput>
                </View>
                <View style={{  marginTop: 5,marginBottom:10 }}>
                  <Text>Mot de passe</Text>
                  <View style={{flexDirection:"row",   borderWidth: 0.5,
                      borderRadius: 5,}}>
                  <TextInput
                    placeholder="Mot de passe"
                    secureTextEntry={this.state.scurite}
                    style={{
                      height: 35,
                      marginTop: 5,
                      paddingVertical: 1,
                      flex:1
                    }}
                    onChangeText={(e)=>{this.setState({
                        password:e
                    })}}
                  ></TextInput>
                   <View style={{width:40,borderTopRightRadius: 5,borderBottomRightRadius:5,alignItems:"center",justifyContent:"center"}}>
                  <TouchableOpacity onPress={()=> this.setState({
                    scurite:!this.state.scurite
                  }) }>
                  <Ionicons name={this.state.scurite ? "eye":"eye-slash"} size={20}  color="#1976d2"/>
                  </TouchableOpacity>
                  </View>
                  </View>
                </View>
             
            </ScrollView>
            <TouchableOpacity onPress={()=> this.login()} style={{height:35,backgroundColor:"#00227b",margin:15,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
             <Text style={{color:"white"}}>Je m'inscris</Text>
               </TouchableOpacity>
             </View>
           </LinearGradient>
           </View>
        )
    }
}

var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
   
     
    }
   
  });
