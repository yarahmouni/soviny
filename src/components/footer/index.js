import React, { Component } from 'react'
import { Text, View,Linking } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Ionicons from 'react-native-vector-icons/FontAwesome';

export default class Footer extends Component {
    render() {
        return (
            <View style={{height:50,
                backgroundColor:"white",
                shadowColor: "#000",
            flexDirection:"row",
           alignItems:"center",
            justifyContent:"center",
shadowOffset: {
	width: 0,
	height: 1,
},
shadowOpacity: 0.20,
shadowRadius: 1.41,

elevation: 2,}}>
{/* <TouchableOpacity 
onPress={()=>{
    Linking.openURL(
        'whatsapp://app'
      );
}}
style={{flex:1}}
>
    <View
style={{flex:1, 
    alignItems:"center",
            justifyContent:"center",
            alignContent:"center",
            height:"100%",
            }}>
            <Ionicons name={"phone"} size={20}  color="#e0e0e0"/>        
            </View>
</TouchableOpacity> */}


<View style={{flex:1, alignItems:"center",
  
  height:"100%",
            justifyContent:"center",}}>
                <TouchableOpacity 
                onPress={()=>{
                    Linking.openURL(`tel:+212666517022`)
                }}
                style={{height:"100%",width:"100%",alignContent:"center",justifyContent:"center"}}>

              
    <Ionicons name={"phone"} size={20}  color="#e0e0e0"/>   
    </TouchableOpacity>
</View>

<View style={{flex:1, alignItems:"center",
  
  height:"100%",
            justifyContent:"center",}}>
                <TouchableOpacity 
                 onPress={()=>{
                    Linking.openURL('whatsapp://send?phone=+212666517022')
                }}
                style={{height:"100%",width:"100%",alignContent:"center",justifyContent:"center"}}>

              
    <Ionicons name={"whatsapp"} size={20}  color="#e0e0e0"/>   
    </TouchableOpacity>
</View>
<View style={{flex:1, alignItems:"center",
  
  height:"100%",
            justifyContent:"center",}}>
                <TouchableOpacity
                   onPress={()=>{
                    Linking.openURL('mailto:contact@soviny.ma')
                }}
                
                style={{height:"100%",width:"100%",alignContent:"center",justifyContent:"center"}}>

              
    <Ionicons name={"envelope"} size={20}  color="#e0e0e0"/>   
    </TouchableOpacity>
</View>
           </View>
        )
    }
}
