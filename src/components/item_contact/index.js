import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Ionicons from 'react-native-vector-icons/FontAwesome5';
export default class ItemContact extends Component {
    render() {
        return (
            <View style={{flex:1}}>
            <View style={{flexDirection:"row",height:60}}>
                <View style={{flex:1,margin:10,}}>
               <View style={{backgroundColor:"#bbdefb",flex:1,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
               <Ionicons name={this.props.icon} size={20}  color="white"/>
               </View>
                </View>
               
                <View style={{flex:4,margin:10,}}>
                <Text style={{color:"black",fontSize:10}}> {this.props.title} </Text>
                <Text style={{fontSize:10}}>{this.props.email} </Text>
                <Text style={{fontSize:10}}>{this.props.phone} </Text>
                </View>
                </View>
            </View>
        )
    }
}
