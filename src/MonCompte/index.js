import React, { Component } from "react";
import { Text, View ,StyleSheet,StatusBar,FlatList,TouchableOpacity} from "react-native";
import {changePassword} from "../servicesApi/authentification/index"
import { getOrderService } from "../servicesApi/order/index";
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/FontAwesome';
import jwt_decode from "jwt-decode";
import moment from 'moment'
import 'moment/locale/fr';
import { ScrollView, TextInput } from "react-native-gesture-handler";
moment.locale('fr');
  const styles = StyleSheet.create({
   
    item: {
        width: "100%",
        height: 60,
        borderBottomWidth: 0.6,
        borderColor: "#fafafa",
        flexDirection: "row",
        shadowColor: "#000",
       
    
     
    },
    title: {
      fontSize: 32,
    },
  });
export default class MonCompte extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            edit:true,
            enableScrollViewScroll:false,
            user:{},
            userId:null,
            token:null,
            newPassword:"",
            scurite:true 
        }}
  componentDidMount() {
    console.log("this=====>",this.props.route.params.user);
    if(this.props.route.params?.user){
      this.setState({
        user:this.props.route.params.user
      })
      var token = this.props.route.params?.user?.token;
      this.setState({
        token:token
      })
var decoded = jwt_decode(token);

const id=decoded.data.user.id
this.setState({userId:id})
    }
   

    this.getOrders()
  }
PassworChange= async()=>{
  if(this.state.newPassword && this.state.newPassword.length>5){
    const res = await changePassword(this.state.newPassword ,this.state.userId, this.state.token);
    console.log("resss==>",res)
    if(res.status===200){
      this.setState({
        newPassword:"",
        edit:true
      })
      alert("Votre mot de passe a été modifié avec succès")
    }
  }else{
    alert("Verifier ton password")
  }
 
}

   Item = ({ item }) => (
    <View style={styles.item}>
      <View style={{}}>

     
        <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
        <Text  style={{color:"red",fontSize:20}}>
            {item?.number}
       
            </Text>
        </View>
        <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
        <Text >{item?.billing?.date_created}</Text>
        </View>
        <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
        <Text >{item?.status}</Text>
        </View>
      
        </View>
    </View>
  );
  getOrders  = async () => {
   const aa= await getOrderService();
  // if(aa.)
console.log("aaaaaa==>",aa.data.length)
let r=[];
if(aa.data.length>0){
  for(let i=0;i<aa.data.length;i++){
    console.log("data=======>",this.state.user?.user_email )
    if(aa.data[i].billing.email === this.state.user?.user_email){
      r.push(aa.data[i])
    }
  
  }

  console.log("data=======>",r)
   this.setState({
       data:r
   })
}
  // const result = aa.data.filter(word => word.email === this.state.user?.user_email);
  // console.log("resssss=>",result)
  //  this.setState({
  //      data:result
  //  })
   }
   renderItem = ({ item }) => (
     <View>
    <View style={styles.item}>
    <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
    <Text  style={{fontSize:10,color:"#2196f3"}}>
        N°{item?.number}
   
        </Text>
    </View>
    <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
    <Text style={{fontSize:10}} > {moment(item?.date_created).format('DD MMMM YYYY')}</Text>
    </View>
    <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
    <Text style={{fontSize:10}} >{item?.status ==="completed"? "Terminée":""}
    {item?.status ==="processing"? "En cours":""}
    {item?.status ==="cancelled"? "Annulée":""}
    </Text>
    </View>
    </View>
    <View style={{height:2,backgroundColor:"#fafafa"}}></View>
</View>
  );

  render() {
    return (
      <View style={{flex:1}}>
      <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={{flex:1}}>
      <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
              
              <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}} >
              <TouchableOpacity
              style={{height:60,width:70,justifyContent:"center"}}
              onPress={()=> this.props.navigation.goBack()}>
              <View style={{alignContent:"center",justifyContent:"center"}}>
               <Ionicons name={"chevron-left"} size={20}  color="white"/>
               </View>
               </TouchableOpacity>
               </View>
               <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}} >
                 <Text style={{color:"white",fontSize:20}}>Mon Compte</Text>
               </View>
               <View style={{alignItems:"center",justifyContent:"center",alignContent:"center",height:60,width:70}} >
            
               </View>
               </View>
               <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
               <View style={{marginHorizontal:10}}>
               <View style={{justifyContent:"center",height:10}}>
                  
                   </View>
               </View>
              <View style={{flex:1}}>
        <ScrollView style={{flex:1,padding:5,}} scrollEnabled={this.state.enableScrollViewScroll}>
        <View style={{flex:1,margin : 10}}>
          <Text style={{fontWeight:"bold"}}>Votre E-mail</Text>
          <Text>{this.state.user && this.state.user.user_email}</Text>
          <Text style={{fontWeight:"bold"}}>Mot de passe</Text>
          <View style={{flexDirection:"row"}}>
            <View style={{flex:3}}>
              {this.state.edit ?
               <Text>***************</Text>:
               <View style={{flexDirection:"row",   borderWidth: 0.5,margin:3,
               borderRadius: 5,}}>
           <TextInput
           secureTextEntry={this.state.scurite}
             placeholder="Mot de passe"
             style={{
               height: 35,
            
               marginTop: 5,
               paddingVertical: 1,
               flex:1
             }}
             onChangeText={(e)=>{this.setState({
              newPassword:e
             })}}
           ></TextInput>
           <View style={{width:40,borderTopRightRadius: 5,borderBottomRightRadius:5,alignItems:"center",justifyContent:"center"}}>
           <TouchableOpacity onPress={()=> this.setState({
             scurite:!this.state.scurite
           }) }>
           <Ionicons name={this.state.scurite ? "eye":"eye-slash"} size={20}  color="#1976d2"/>
           </TouchableOpacity>
           </View>
           </View>

              }
           
            </View>
            <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
            {this.state.edit ?
            <TouchableOpacity
        
           onPress={()=> {
             this.setState({
               edit:!this.state.edit
             })
           }}>
          <Ionicons name={"edit"} size={20}  color="#1e88e5"/>
          </TouchableOpacity>:
      
            <TouchableOpacity
        
           onPress={()=> {
             this.setState({
               edit:!this.state.edit
             })
           }}>
          <Ionicons name={"eraser"} size={20}  color="#1e88e5"/>
          </TouchableOpacity>

  }
          </View>
          </View>
          { this.state.edit ? null :
          <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:10}}>
           
           <TouchableOpacity onPress={()=>{this.PassworChange()}} style={{ height: 35,width:100,backgroundColor:"#1e88e5",borderRadius:5,alignItems:"center",justifyContent:"center"}}>
   <Text style={{color:"white"}}>Sauvegarder</Text>
            </TouchableOpacity>
          </View>
         

          }
        
        </View>
        <View style={{flex:2}}>
        <View style={{height:1.5,backgroundColor:"#2196f3",margin : 10}}>

        </View>

        <View>
          <Text style={{color:"#2196f3",fontSize:20}}>Mes commandes</Text>
        </View>
        
        <View
          style={{
            marginBottom:5,
           height:300,
           margin:5,
            borderColor: "#fafafa",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,

            elevation: 2,
            borderWidth: 1,
            borderRadius: 5,
          }}
        >
          <View
            style={{
              width: "100%",
              height: 60,
              borderBottomWidth: 1,
              borderColor: "#fafafa",
              flexDirection: "row",
            
             
            }}
          >
            <View style={{ flex: 1 ,alignContent:"center",justifyContent:"center" ,alignItems:'center'}}>
            
              <Text>Commande</Text>
            </View>
            <View style={{ flex: 1 ,alignContent:"center",justifyContent:"center" ,alignItems:'center'}}>
             
              <Text>Date</Text>
            </View>
            <View style={{ flex: 1 ,alignContent:"center",justifyContent:"center" ,alignItems:'center'}}>
           
              <Text>État</Text>
            </View>
          </View>{
            this.state.data && this.state.data.length>0 &&
            <View>
             <FlatList
             style={{marginBottom:5}}
             onStartShouldSetResponderCapture={() => {
     this.setState({ enableScrollViewScroll: false });
     if (this.refs.myList.scrollProperties.offset === 0 && this.state.enableScrollViewScroll === false) {
       this.setState({ enableScrollViewScroll: true });
     }
 }}
            data={this.state.data}
            renderItem={this.renderItem}
            keyExtractor={item => JSON.stringify(item.id)}
          />
          </View> 
          }
         
        </View>
        </View>
        </ScrollView>
        </View>
        </View>
        </LinearGradient>

      </View>
    );
  }
}
