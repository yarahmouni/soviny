import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Dimensions,
} from "react-native";
import Ionicons from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import { ScrollView } from "react-native-gesture-handler";
import ItemContact from "../components/item_contact";
import { SocialIcon, Button } from "react-native-elements";
import {RestPasswordService} from '../servicesApi/authentification'
const { width, height } = Dimensions.get("window");
export default class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
         email:"",
         customer_note:""
        };
      }
    reset=async()=>{
        const res=  await    RestPasswordService(this.state.email);
        console.log("res login====>",res)
      if(res.data){
          if(res.data.status=="ok"){
            alert("Nous vous avons envoyé un email")
              this.props.navigation.navigate("Signin",{"item":this.props.route?.params?.item,"customer_note":this.state.customer_note})
             
          }else{
              alert("Verifier votre email !")
          }
      }else{
          alert("verifier votre email !") 
      }
      
      
            }
            componentDidMount(){
                if( this.props.route?.params && this.props.route?.params?.customer_note){
                  this.setState({
                    customer_note:this.props.route?.params?.customer_note
                  })
                }
              }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <LinearGradient
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          colors={["#4dbeff", "#45a7ee", "#3c8fdc"]}
          style={styles.linearGradient}
        >
          <View
            style={{
              height: 70,
              justifyContent: "space-between",
              padding: 10,
              flexDirection: "row",
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <TouchableOpacity
                style={{ height: 60, width: 70, justifyContent: "center" }}
                onPress={() => this.props.navigation.goBack()}
              >
                <View
                  style={{ alignContent: "center", justifyContent: "center" }}
                >
                  <Ionicons name={"chevron-left"} size={20} color="white" />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <Text
                style={{ color: "white", fontSize: 20, fontWeight: "bold" }}
                numberOfLines={1}
                ellipsizeMode="tail"
              >
                A Propos
              </Text>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
                height: 60,
                width: 70,
              }}
            ></View>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "white",
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            }}
          >
           <ScrollView style={{marginTop: 30,flex:1,padding:30,alignContent:"center"}}>
             
           
               
              <View style={{  margin:5}}>
                  <Text>Email</Text>
                  <TextInput
                    placeholder="Email"
                    style={{
                      height: 35,
                      borderWidth: 0.5,
                      borderRadius: 5,
                      marginTop: 5,
                      paddingVertical: 1,
                    }}
                    onChangeText={(e)=>{this.setState({
                        email:e
                    })}}
                  ></TextInput>
                </View>
                <View>
                <TouchableOpacity onPress={()=> {
                     this.reset()
                    }} style={{height:35,backgroundColor:"#00227b",margin:15,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
             <Text style={{color:"white"}}>Réinitialiser le mot de passe</Text>
               </TouchableOpacity>
                </View>
                 
             
              
              </ScrollView>
           
          </View>
        </LinearGradient>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
});
