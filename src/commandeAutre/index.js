/* eslint-disable no-console */
import React from 'react';
import { AppRegistry, StyleSheet, Text, TouchableOpacity, View,Image,TextInput } from 'react-native';
// eslint-disable-next-line import/no-unresolved
import { RNCamera ,RNFS} from 'react-native-camera';
import { ScrollView } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/FontAwesome';

import LinearGradient from 'react-native-linear-gradient';




export default class CommandeAutre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          image:null
        };
      }
    render() {
        return (
            <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={{flex:1}}>
            

            <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
            <TouchableOpacity
            style={{height:60,width:70,justifyContent:"center"}}
    activeOpacity={0.8}  onPress={()=> this.props.navigation.goBack()}>
           <View    style={{alignContent:"center",justifyContent:"center"}}>
          
            <Ionicons name={"chevron-left"} size={20}  color="white"/>
            </View>
            </TouchableOpacity>
            <View style={{alignContent:"center",justifyContent:"center"}}>
        {/* <Text style={{color:"white",fontSize:20}}>{this.props.route.params.item.titre}</Text> */}
               </View>
            <View></View>
            
            
            </View>


         <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
         <View style={{justifyContent:"center",height:50,alignItems:"center",marginTop:5}}>
                <Text style={{fontSize:20,color:"black"}}>
                 Probléme identifié
                </Text>
                </View>
                <View style={{flex:1,margin:15,}}>
                  <View style={{flex:1,justifyContent:"center"}}>
                   <View style={{borderColor:"#9e9e9e",borderWidth:1,height:150,white:100,borderRadius:15,alignItems:"center",justifyContent:"center"}}>
                       <Text>
                           J'ai un problème de coffre qui ne s'ouvre pas
                       </Text>
                   </View>
                   <View style={{flex:1,justifyContent:"center",alignItems:"center",margin:15}}>
                       <Text style={{fontSize:25,color:"#3c8fdc"}}> 
                           L'estimation du tarif se fera 
                       </Text>
                       <Text style={{fontSize:25,color:"#3c8fdc"}}> 
                           directement avec 
                       </Text>
                       <Text style={{fontSize:25,color:"#3c8fdc"}}> 
                           le professionnel
                       </Text>
                       

</View>
                  </View>
                  <View style={{height:50,backgroundColor:"#284993",borderRadius:10,justifyContent:"center",alignItems:"center"}}>
                      <Text style={{color:"white",fontSize:17}}>Commander</Text>
                  </View>
                </View>
              
        </View>
          
          </LinearGradient>
        );
      }
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
      },
      preview: {
        flex: 1,
        height:100,
        backgroundColor:"red",
        justifyContent: 'flex-end',
        alignItems: 'center',
      },
      capture: {
          width:"80%",
          alignItems:"center",
        backgroundColor:"#283593",
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
      },
    });
    