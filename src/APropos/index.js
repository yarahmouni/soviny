import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Dimensions,
} from "react-native";
import Ionicons from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import { ScrollView } from "react-native-gesture-handler";
import ItemContact from "../components/item_contact";
import { SocialIcon, Button } from "react-native-elements";
const { width, height } = Dimensions.get("window");
export default class APropos extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <LinearGradient
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          colors={["#4dbeff", "#45a7ee", "#3c8fdc"]}
          style={styles.linearGradient}
        >
          <View
            style={{
              height: 70,
              justifyContent: "space-between",
              padding: 10,
              flexDirection: "row",
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <TouchableOpacity
                style={{ height: 60, width: 70, justifyContent: "center" }}
                onPress={() => this.props.navigation.goBack()}
              >
                <View
                  style={{ alignContent: "center", justifyContent: "center" }}
                >
                  <Ionicons name={"chevron-left"} size={20} color="white" />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <Text
                style={{ color: "white", fontSize: 20, fontWeight: "bold" }}
                numberOfLines={1}
                ellipsizeMode="tail"
              >
                A Propos
              </Text>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
                height: 60,
                width: 70,
              }}
            ></View>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "white",
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            }}
          >
            <View style={{alignItems:'center',justifyContent:"center",marginTop:10}}>
            <Text style={{fontSize:20,color:"#3c8fdc",fontWeight:"bold"}}>Soviny </Text>  
            </View>
           <ScrollView style={{marginTop: 20,}}>
              <View style={{  marginHorizontal: 20,flex:1, height:"100%" }}>
              <View style={{flex:1}}>
               
                  <Text style={{alignItems:"center",textAlign:"center"}}>
                  SOVINY est une plateforme spécialisée dans les prestations de réparation, de dépannage et de bricolage à domicile ou au bureau.
                  </Text>
                  <Text style={{alignItems:"center",textAlign:"center",marginTop:20}}>
                 Le but de cette solution est de fournir aux clients particuliers ou entreprises des prestations de qualité permettant de répondre à leurs attentes en matière de réparation, bricolage et service en respectant une qualité de prestation et un tarif honnête et transparent.
                  </Text>
                  <Text style={{alignItems:"center",textAlign:"center",marginTop:35}}>
                  En quelques clics, vous envoyez votre demande. Notre centre d'appel valide avec vous les détails de vos besoins et confirme un rendez-vous selon votre disponibilité pour les travaux souhaités.
                    </Text>
                    <Text style={{alignItems:"center",textAlign:"center",marginTop:35,marginBottom:20}}>
                    SOVINY  vous accompagne dans la réalisation de votre projet du simple au plus complexe grâce à une équipe d'experts avec des dizaines d'années d'expériences
                      </Text>
                 
              </View>
              </View>
              </ScrollView>
              <TouchableOpacity onPress={()=> this.props.navigation.navigate("Contacter") } style={{height:35,backgroundColor:"#00227b",margin:15,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
             <Text style={{color:"white"}}>Nous contacter</Text>
               </TouchableOpacity>
           
          </View>
        </LinearGradient>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
});
