import React, { Component } from 'react'
import { Text, View ,StyleSheet,FlatList, ImageBackground,Dimensions,ScrollView,TextInput} from 'react-native'
import Ionicons from 'react-native-vector-icons/FontAwesome';

import LinearGradient from 'react-native-linear-gradient';
import { TouchableHighlight } from 'react-native-gesture-handler';

  const {width,height} = Dimensions.get('window');

export default class Autre extends Component {

    
 

            
    render() {
      //alert(JSON.stringify(this.props.route.params.item))
        return (
            <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={styles.linearGradient}>
            <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
            <TouchableHighlight  onPress={()=> this.props.navigation.goBack()}>
           <View style={{alignContent:"center",justifyContent:"center"}}>
            <Ionicons name={"chevron-left"} size={20}  color="white"/>
            </View>
            </TouchableHighlight>
            <View style={{alignContent:"center",justifyContent:"center"}}>
        <Text style={{color:"white",fontSize:20}}>{this.props.route.params.item.titre}</Text>
               </View>
            <View></View>
            
            
            </View>
            <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
            <View style={{marginHorizontal:30}}>
            <View style={{justifyContent:"center",height:50}}>
                <Text style={{fontSize:15,color:"black"}}>
                Quel est Votre besoin ?
                </Text>
                </View>
            </View>
            <View style={{flex:1,margin:10}}>
            <TextInput
            style={{height:"55%",borderColor:"#9e9e9e",borderWidth:1,borderRadius:10}}
    multiline={true}
    numberOfLines={4}
    />
    <View style={{height:60,backgroundColor:"#283593",marginTop:20,alignContent:"center",alignItems:"center",justifyContent:"center",borderRadius:5}}>
<Text style={{color:"white",fontSize:30}}>Ok</Text>
    </View>
            </View>
            </View>
          </LinearGradient>
        )
    }
}
var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
   
     
    },
    buttonText: {
      fontSize: 18,
  
      textAlign: 'center',
      margin: 10,
      color: '#ffffff',
      backgroundColor: 'transparent',
    },
  });
