
import React, { Component } from 'react'
import { Text, View ,Image,TouchableOpacity,StyleSheet,TextInput} from 'react-native'
import Ionicons from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';
import {LoginService} from "../servicesApi/authentification/index"

export default class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = {
         email:"",
         password:"",
         customer_note:"",
         scurite:true 
        };
      }
      login=async()=>{
  const res=  await    LoginService(this.state.email,this.state.password);
  console.log("res login====>",res)
if(res.data){
    if(res.status==200){
     
      if(this.props.route.params?.old_route==="Home"){
        this.props.navigation.navigate("MonCompte",{"user":res.data})
      }else{
        this.props.navigation.navigate("PasserCommande",{"email":res.data.user_email,"item":this.props.route.params.item,"customer_note":this.state.customer_note})
      }
      
       
    }else{
        alert("Verifier votre email et  mot de passe")
    }
}else{
    alert("verifier votre email et  mot de passe") 
}


      }
      componentDidMount(){
        console.log("this=====>",this.props)
        if(this.props.route?.params?.customer_note){
          this.setState({
            customer_note:this.props.route?.params?.customer_note
          })
        }
      }

    render() {
        return (
            <View style={{flex:1}}>
            <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={styles.linearGradient}>
            <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
              
           <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}} >
           <TouchableOpacity
           style={{height:60,width:70,justifyContent:"center"}}
           onPress={()=> this.props.navigation.goBack()}>
           <View style={{alignContent:"center",justifyContent:"center"}}>
            <Ionicons name={"chevron-left"} size={20}  color="white"/>
            </View>
            </TouchableOpacity>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}} >
              <Text style={{color:"white",fontSize:20}}>Je me connecte</Text>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center",height:60,width:70}} >
         
            </View>
            </View>
            <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
            <View style={{marginHorizontal:30}}>
            <View style={{justifyContent:"center",height:50}}>
                <Text style={{fontSize:15,color:"black"}}>
                 Je me connecte
                </Text>
                </View>
            </View>
            <ScrollView style={{flex:1,marginHorizontal:20}}>
            <View style={{  marginTop: 5 }}>
                  <Text>Email</Text>
                  <TextInput
                    placeholder="Email"
                    style={{
                      height: 35,
                      borderWidth: 0.5,
                      borderRadius: 5,
                      marginTop: 5,
                      paddingVertical: 1,
                    }}
                    onChangeText={(e)=>{this.setState({
                        email:e
                    })}}
                  ></TextInput>
                </View>
                <View style={{  marginTop: 5,marginBottom:10 }}>
                  <Text>Mot de passe</Text>
                  <View style={{flexDirection:"row",   borderWidth: 0.5,
                      borderRadius: 5,}}>
                  <TextInput
                  secureTextEntry={this.state.scurite}
                    placeholder="Mot de passe"
                    style={{
                      height: 35,
                   
                      marginTop: 5,
                      paddingVertical: 1,
                      flex:1
                    }}
                    onChangeText={(e)=>{this.setState({
                        password:e
                    })}}
                  ></TextInput>
                  <View style={{width:40,borderTopRightRadius: 5,borderBottomRightRadius:5,alignItems:"center",justifyContent:"center"}}>
                  <TouchableOpacity onPress={()=> this.setState({
                    scurite:!this.state.scurite
                  }) }>
                  <Ionicons name={this.state.scurite ? "eye":"eye-slash"} size={20}  color="#1976d2"/>
                  </TouchableOpacity>
                  </View>
                  </View>
                  <View style={{alignItems:"center",justifyContent:'center',marginTop:10}}>
                  <TouchableOpacity onPress={()=>
                    this.props.navigation.navigate("ResetPassword",{"item":this.props.route.params.item,"customer_note":this.state.customer_note})
                     } style={{height:35,alignItems:"center",justifyContent:"center"}}>
               
                    <Text style={{color:"#1976d2"}}>Mot de passe oublié ?</Text>
                    </TouchableOpacity>
                  </View>
                </View>
             
            </ScrollView>
            <TouchableOpacity onPress={()=> this.login()} style={{height:35,backgroundColor:"#00227b",margin:15,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
             <Text style={{color:"white"}}>Je me connecte</Text>
               </TouchableOpacity>
             </View>
           </LinearGradient>
           <View style={{height:40,shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 1,
},
shadowOpacity: 0.20,
shadowRadius: 1.41,

elevation: 2,}}>

           </View>
           </View>
        )
    }
}

var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
   
     
    }
   
  });
