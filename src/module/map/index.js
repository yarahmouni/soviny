//import liraries
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  Alert,
  TouchableHighlight,
  Platform,
  PermissionsAndroid,
  Dimensions,
  Button
} from 'react-native';
import Ionicons from "react-native-vector-icons/FontAwesome";
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Geolocation from 'react-native-geolocation-service';
const {width,height}= Dimensions.get('window');
// create a component
class CustomMap extends Component {
  state = {
  
      forceLocation: true,
      highAccuracy: true,
      loading: false,
      showLocationDialog: true,
      significantChanges: false,
      updatesEnabled: false,
      foregroundService: false,
      location: {},
    
    modalVisible: true,
    region: {
      latitude: 36.51992535,
      longitude: 10.0196737089387,
       latitudeDelta: 0.015,
       longitudeDelta: 0.0121,
    },
    x: {latitude: 36.51992535, longitude: 10.0196737089387},
  };
  componentDidMount() {
    console.log('load map');
  }

  findLocalitation() {
    return this.props.data(this.state.x);
    this.closeMap();
  }

  closeMap() {
    this.props.isOpen(fasle);
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  getLocation = async () => {
    const hasLocationPermission = await this.hasLocationPermission();

    if (!hasLocationPermission) {
      return;
    }

    this.setState({ loading: true }, () => {
      Geolocation.getCurrentPosition(
        (position) => {
          const coords=position.coords
          coords.latitudeDelta=0.015;
          coords.longitudeDelta=0.0121;
          this.setState({ region: coords, loading: false,x:coords });
          console.log(position);
        },
        (error) => {
          this.setState({ loading: false });
          Alert.alert(`Code ${error.code}`, error.message);
          console.log(error);
        },
        {
          accuracy: {
            android: 'high',
            ios: 'best',
          },
          enableHighAccuracy: this.state.highAccuracy,
          timeout: 15000,
          maximumAge: 10000,
          distanceFilter: 0,
          forceRequestLocation: this.state.forceLocation,
          showLocationDialog: this.state.showLocationDialog,
        },
      );
    });
  };

  hasLocationPermission = async () => {
    if (Platform.OS === 'ios') {
      const hasPermission = await this.hasLocationPermissionIOS();
      return hasPermission;
    }

    if (Platform.OS === 'android' && Platform.Version < 23) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) {
      return true;
    }

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    }

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show(
        'Location permission denied by user.',
        ToastAndroid.LONG,
      );
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show(
        'Location permission revoked by user.',
        ToastAndroid.LONG,
      );
    }

    return false;
  };
  render() {
    const {
      forceLocation,
      highAccuracy,
      loading,
      location,
      showLocationDialog,
      significantChanges,
      updatesEnabled,
      foregroundService,
    } = this.state;
    return (
        <Modal
        onRequestClose={() => {}}
        animationType="slide"
        transparent={true}
        visible={this.props.isOpen}
        style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.8)',
          }}>
          <View
            style={{
              width: width - 30,
              justifyContent: 'center',
              alignItems: 'flex-end',
              marginTop: 30,
            }}>
            <TouchableHighlight
              onPress={() => {
                this.props.close();
                this.setState({
                  modalVisible: false,
                });
              }}
              style={{
                height: 25,
                width: 25,
                borderRadius: 25,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center',
                alignContent: 'center',
              }}>
              <Text style={{color: 'black', textAlign: 'center'}}>X</Text>
            </TouchableHighlight>
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              alignContent: 'center',
              flex: 1,
              borderRadius: 5,
            }}>
            <MapView
              initialCamera={{
                altitude: 15000,
                center: {
                  latitude: 23.7603,
                  longitude: 90.4125,
                },
                heading: 0,
                pitch: 0,
                zoom: 11,
              }}
              initialRegion={this.state.region}
              region={this.state.region}
              onRegionChange={this.onRegionChange}
              style={{height: 400, width: 300}}
              onPress={event => {
                let d = {
                  latitude: event.nativeEvent.coordinate.latitude,
                  longitude: event.nativeEvent.coordinate.longitude,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
                };

                this.setState({
                  x: event.nativeEvent.coordinate,
                  region: d,
                });
              }}>
              <Marker draggable coordinate={this.state.x} />
            </MapView>
            <TouchableHighlight
              onPress={() => {
                this.getLocation()
              }}
              style={{
                position:"absolute",
                height: 40,
                width: 40,
                right:15,
                borderRadius:20,
                bottom:70,
                backgroundColor: "white",
                marginTop: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
                  <Ionicons name={"crosshairs"} size={20} color="black" />
            
            </TouchableHighlight>
         
            <TouchableHighlight
              onPress={() => {
                this.findLocalitation();
                this.props.close();
                this.setState({modalVisible: false});
              }}
              style={{
                height: 30,
                width: 150,
                backgroundColor: "#283593",
                marginTop: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: 'white',
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  fontSize: 12,
                }}>
                Valide
              </Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});

//make this component available to the app
export default CustomMap;
