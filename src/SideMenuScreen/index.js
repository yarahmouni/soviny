import React, { Component } from 'react'
import { Text, View ,TouchableOpacity,StyleSheet,  Image,
    PixelRatio,} from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import MenuButton from './MenuButton'
const aideImg = require('../../src/assets/watsap.png')
export default class SideMenu extends Component {
  openAccount = (params) => this.openModal('Home',params)
  openContacter = () => this.openModal('Contacter')
  openAPropos = () => this.openModal('APropos')
  openMonCompte = () => this.openModal('Signin')
  
  openMyBookings = () => 
  {alert(hello)}
  openModal = (screen) => {
    if(screen==="Signin"){
      this.props.navigation.navigate("Signin",{  old_route:"Home"})
    }else{
      this.props.navigation.navigate(screen)
    }
  
  }
    render() {
        return (
            
          <View style={{backgroundColor:'#3f51b5',flex:1}}>
              <View style={{ flex: 1, paddingTop: 20 }}>
              <MenuButton
              text={"Accueil"}
              image={"power-off"}
              onPress={this.openAccount}
            />
              <MenuButton
              text={"Mon Compte"}
              image={"user-circle"}
              onPress={this.openMonCompte}
            />
              <MenuButton
              text={"A Propos"}
              image={"info"}
              onPress={this.openAPropos}
            />
              <MenuButton
              text={"Contact"}
              image={"phone-alt"}
              onPress={this.openContacter}
            />

              </View>
            {/* <LinearGradient style={{flex:1}}  start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']}>
                <TouchableOpacity 
              onPress={onPress} testID={testID}
               >
    <View style={[styles.container]}>
      <View style={styles.imageWrapper}>
        
      </View>
      <Text style={styles.text}>Mon Compte</Text>
    </View>
  </TouchableOpacity>
            </LinearGradient> */}
  </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      height: 63,
      paddingRight: 5,
    },
    withBorder: {
      borderBottomWidth: 1 / PixelRatio.get(),
      borderColor: 'white',
    },
    text: {
      backgroundColor: 'transparent',
      color: 'white',
      fontSize: 16,
    },
    imageWrapper: {
      width: 62,
      alignItems: 'center',
      justifyContent: 'center',
    },
    icon: {
      maxWidth: 20,
      maxHeight: 20,
      resizeMode: 'contain',
    },
  });
