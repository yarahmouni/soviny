import PropTypes from 'prop-types';
import React from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  PixelRatio,
  Text
} from 'react-native';
import Ionicons from "react-native-vector-icons/FontAwesome5";
// import Colors from '../../../common/Colors';
// import FixedText from '../../../common/components/FixedText';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 75,
    paddingRight: 5,
  },
  withBorder: {
    borderBottomWidth: 1 / PixelRatio.get(),
    borderColor: 'white',
  },
  text: {
    backgroundColor: 'transparent',
    color: "white",
    fontSize: 16,
  },
  imageWrapper: {
    width: 62,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    maxWidth: 20,
    maxHeight: 20,
    resizeMode: 'contain',
  },
});

const MenuButton = ({ hideBorder, text, image, onPress, testID }) => (
  <TouchableOpacity onPress={onPress} testID={testID}>
    <View style={[styles.container, hideBorder ? null : styles.withBorder]}>
      <View style={styles.imageWrapper}>
        {/* <Image style={styles.icon} source={image} /> */}
        <View style={{backgroundColor:"white",borderRadius:60,height:30,width:30,alignItems:"center",justifyContent:'center'}}>
        <Ionicons name={image ?image:"universal-access"} size={20}  color="#3f51b5" />
        </View>
      </View>
      <Text style={{color:"white"}}>{text}</Text>
    </View>
  </TouchableOpacity>
);

MenuButton.propTypes = {
  hideBorder: PropTypes.bool,
  text: PropTypes.string,
  image: PropTypes.number,
  onPress: PropTypes.func,
  testID: PropTypes.string,
};

export default MenuButton;
