import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  ImageBackground,
  Dimensions,
  TouchableHighlight,
  Image,
  TouchableOpacity,
} from "react-native";
import Ionicons from "react-native-vector-icons/FontAwesome";
import { getProductTags, getImageProductTags } from "../servicesApi/index";
import { getCategoriesService } from "../servicesApi/categoriesService/index";
import LinearGradient from "react-native-linear-gradient";
import { SafeAreaView } from "react-native-safe-area-context";
import { ScrollView } from "react-native-gesture-handler";

const { width, height } = Dimensions.get("window");

export default class VotreCommande extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categorise: [],
      customer_note: "",
    };
  }

  componentDidMount() {
    if (this.props.route?.params?.customer_note) {
      this.setState({
        customer_note: this.props.route?.params?.customer_note,
      });
    }

    console.log("rrrr====>", this.props.route.params.item);
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <LinearGradient
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          colors={["#4dbeff", "#45a7ee", "#3c8fdc"]}
          style={styles.linearGradient}
        >
          <View
            style={{
              height: 70,
              justifyContent: "space-between",
              padding: 10,
              flexDirection: "row",
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <View
                  style={{ alignContent: "center", justifyContent: "center" }}
                >
                  <Ionicons name={"chevron-left"} size={20} color="white" />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <Text style={{ color: "white", fontSize: 15 }}>
                Votre Commande
              </Text>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            ></View>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "white",
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            }}
          >
            <View style={{ flex: 1, marginHorizontal: 15 }}>
              <View style={{ justifyContent: "center", height: 90 }}>
                <Text style={{ color: "#3c8fdc", fontSize: 20 }}>
                  Bienvenue
                </Text>
                <View style={{ marginTop: 15 }}>
                  <Text style={{ fontSize: 15, fontWeight: "bold" }}>
                    Connectez-vous ou créez un compte
                  </Text>
                </View>
              </View>
              <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 2 }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("Signin", {
                        item: this.props.route.params.item,
                        customer_note: this.state.customer_note,
                      });
                    }}
                    style={{
                      backgroundColor: "#3849aa",
                      height: 35,
                      borderRadius: 5,
                      alignItems: "center",
                      justifyContent: "center",
                      marginVertical: 10,
                    }}
                  >
                    <Text style={{ color: "white" }}>Se connecter</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("SiginUp", {
                        item: this.props.route.params.item,
                        customer_note: this.state.customer_note,
                        old_route:"VotreCommande"
                      });
                    }}
                    style={{
                      backgroundColor: "#e8eaf6",
                      height: 35,
                      borderRadius: 5,
                      alignItems: "center",
                      justifyContent: "center",
                      marginVertical: 10,
                    }}
                  >
                    <Text style={{ color: "#3849aa" }}>Créer un compte</Text>
                  </TouchableOpacity>
                  {/* <View
                    style={{
                      backgroundColor: "#3f51b5",
                      height: 35,
                      borderRadius: 5,
                      alignItems: "center",
                      justifyContent: "center",
                      marginVertical: 10,
                    }}
                  >
                    <Text style={{ color: "white" }}>
                      Se connecter avec Facebook
                    </Text>
                  </View> */}
                  {/* <View
                    style={{
                      backgroundColor: "#82b1ff",
                      height: 35,
                      borderRadius: 5,
                      alignItems: "center",
                      justifyContent: "center",
                      marginVertical: 10,
                    }}
                  >
                    <Text style={{ color: "white" }}>
                      connecter-vous avec Google
                    </Text>
                  </View> */}
                </View>

                {/* <View style={{height:20,flexDirection:"row",alignItems:"center",marginTop:10}}>
              <View style={{height:2,flex:1, backgroundColor:"#f5f5f5"}}></View>
              <View style={{height:30,width:20,alignItems:"center",justifyContent:"center"}}>
                  <Text style={{color:"#9e9e9e"}} >OU</Text>
              </View>
              <View style={{height:2,flex:1,backgroundColor:"#f5f5f5"}}></View>
            </View>
            <View style={{flex:1,marginTop:10}}>
                <View style={{flex:1}}>
        <View style={{backgroundColor:"#1976d2",height:35,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
            <Text style={{color:"white"}}>Connectez-vous avec votre e-mail</Text>
        </View>
        </View>
        <View style={{flex:1,marginTop:5}}>
        <View style={{backgroundColor:"#3949ab",height:35,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
            <Text style={{color:"white"}}>Suivant</Text>
        </View>
        </View> */}

                {/* </View> */}
              </ScrollView>
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  }
}
var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
});
