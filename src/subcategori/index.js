import React, { Component } from 'react'
import { Text, View ,StyleSheet,FlatList, ImageBackground,Dimensions,ScrollView,TouchableOpacity} from 'react-native'
import Ionicons from 'react-native-vector-icons/FontAwesome';
import {getSubCategoriesService} from '../servicesApi/subCategorieSerivce/index'
import LinearGradient from 'react-native-linear-gradient';
import { TouchableHighlight } from 'react-native-gesture-handler';
import Footer from '../components/footer';

  const {width,height} = Dimensions.get('window');

export default class SubCategori extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subcategorise:[]
    };
  }

  getP= async(id)=>{
    const res= await getSubCategoriesService(id)
    console.log("reeeee====<===<>",res)
    if(res.status ===200){
      this.setState({
        subcategorise:res.data
      })
      console.log(res.data)
      
    }
   
 
   }
   componentDidMount(){
    if(this.props.route.params.item && this.props.route.params.item.id){
      const id =this.props.route.params.item.id
      this.getP(id);
    }
  
  }
      renderItem = ({item}) =>  (
        <TouchableOpacity
        activeOpacity={0.8}
            onPress={()=>{
             
              this.props.navigation.navigate("Commander",{item,"last":this.props.route.params.item.name})
              }}>
         <View style={{ width: ((width-35)/3), margin:10,alignContent:"center",justifyContent:"center",alignItems:"center"}} key={item}>
           <View>
            <ImageBackground
          resizeMode="stretch"
style={{
    margin:2,
    width: ((width-30)/3),
    height: ((width-35)/3),
    alignItems: 'center',
    justifyContent: 'center',
   
  }}
            source={{
                uri:    item && item.image !=null ? item.image.src  :'https://soviny.happyagency.biz/wp-content/uploads/2020/12/electricite-panneau-solaire-120x120-1.png',
              }}
            />
            </View>
            <View style={{height:"10%",alignItems:"center",justifyContent:"center",alignContent:"center"}}>
             <Text style={{textAlign:"center",fontSize:15,color:"black", fontWeight: "bold"}}>
                {item.name}
            </Text>
            </View>
        </View>
        </TouchableOpacity>
      
      )

            
    render() {
      //alert(JSON.stringify(this.props.route.params.item))
        return (
            <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={styles.linearGradient}>
            <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
            <TouchableHighlight 
            style={{height:60,width:40,justifyContent:"center"}}
            onPress={()=> this.props.navigation.goBack()}>
           <View style={{alignContent:"center",justifyContent:"center"}}>
            <Ionicons name={"chevron-left"} size={20}  color="white"/>
            </View>
            </TouchableHighlight>
            <View style={{alignContent:"center",justifyContent:"center",fontWeight: 'bold'}}>
        <Text style={{color:"white",fontSize:20,fontWeight: 'bold'}} numberOfLines={1} ellipsizeMode='tail'> {this.props.route.params.last}/{this.props.route.params.item.name}</Text>
               </View>
            <View></View>
            
            
            </View>
            <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
            <View style={{marginHorizontal:30}}>
            <View style={{justifyContent:"center",height:50}}>
                <Text style={{fontSize:15,color:"black"}}>
                Quel est Votre besoin ?
                </Text>
                </View>
            </View>
            <View style={{flex:1}}>
            <FlatList
        data={this.state.subcategorise}
        numColumns={3}
        renderItem={this.renderItem}
        keyExtractor={item => item.id}
      />
          
            </View>
            <Footer></Footer>
            </View>
          </LinearGradient>
        )
    }
}
var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
   
     
    },
    buttonText: {
      fontSize: 18,
  
      textAlign: 'center',
      margin: 10,
      color: '#ffffff',
      backgroundColor: 'transparent',
    },
  });
