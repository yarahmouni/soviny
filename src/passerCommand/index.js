import React, { Component } from "react";
import { Text, View, StyleSheet, Dimensions, Modal } from "react-native";
import Ionicons from "react-native-vector-icons/FontAwesome";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import validate from './validation';
import LinearGradient from "react-native-linear-gradient";
import { cerateOrderService } from "../servicesApi/order/index";
import {
  TextInput,
  ScrollView,
  TouchableOpacity,
} from "react-native-gesture-handler";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import CustomMap from "@customMap/index";
const { width, height } = Dimensions.get("window");

export default class PasserCommande extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categorise: [],
      type: "date",
      show: false,
      currentDate: "",
      date: new Date(),
      heur: null,
      dateReservation: null,
      first_name: "",
      last_name: "",
      company: "",
      address_1: "",
      city: "",
      country: "MA",
      email: "",
      phone: "",
      isOpen: false,
      lat: 0,
      lng: 0,
      first_name_Errors:[],
      tel_Errors:[],
      address_Errors:[],
      ville_Errors:[],
      customer_note:""

    };
  }

  getDetaille(e) {
    console.log("laltidude", e.latitude);
    console.log("laltidude", e.longitude);
    this.setState({
      lat: e.latitude,
      lng: e.longitude,
    });
  }
  componentDidMount() {
    console.log("aaa=>",this.props.route)
    if(this.props.route?.params?.customer_note){
      this.setState({
        customer_note:this.props.route?.params?.customer_note
      })
      console.log("aaa=>",this.props.route?.params?.customer_note)
    }
  }
 

  onChange = async (event, selectedDate) => {
    this.setState({
      show: false,
    });
    const date = new Date(event.nativeEvent.timestamp);

    if (this.state.type === "time") {
      this.setState({
        date: new Date(event.nativeEvent.timestamp),
        heur: moment(new Date(event.nativeEvent.timestamp)).format("HH:mm"),
      });
    }
    if (this.state.type === "date") {
      this.setState({
        date: new Date(event.nativeEvent.timestamp),
        dateReservation: moment(new Date(event.nativeEvent.timestamp)).format(
          "DD/MM/YY"
        ),
      });
    }
  };

  showDatepicker = (type) => {
    this.setState({
      show: true,
      type: type,
    });
  };
  testValidet= async()=>{
    const fistNameErrorsvalidete= validate.validateNom(this.state.first_name)
    if (fistNameErrorsvalidete.length > 0) {
      let firstNameErrors = [];
      firstNameErrors.push(fistNameErrorsvalidete);
      this.setState({
        first_name_Errors:firstNameErrors,
       
      });

    }else{
      this.setState({
        first_name_Errors:[],
       
      });
    }
    /// telephone
    const telephoneErrorsvalidete= validate.validatetelephone(this.state.phone)
    if (telephoneErrorsvalidete.length > 0) {
      let phoneErrors = [];
      phoneErrors.push(telephoneErrorsvalidete);
      this.setState({
        tel_Errors:phoneErrors,
       
      });

    }else{
      this.setState({
        tel_Errors:[],
       
      });
    }
/// vaildet address
const addressErrorsvalidete= validate.validateAddress(this.state.address_1)
if (addressErrorsvalidete.length > 0) {
  let addressErrors = [];
  addressErrors.push(addressErrorsvalidete);
  this.setState({
    address_Errors:addressErrors,
   
  });

}else{
  this.setState({
    address_Errors:[],
   
  });
}
// ville ville_Errors
const villeErrorsvalidete= validate.validateVille(this.state.address_1)
if (villeErrorsvalidete.length > 0) {
  let villeErrors = [];
  villeErrors.push(villeErrorsvalidete);
  this.setState({
    ville_Errors:villeErrors,
   
  });

}else{
  this.setState({
    ville_Errors:[],
   
  });
}
    




  }
  Suivant = async () => {
 await  this.testValidet();
  if( this.state.first_name_Errors.length ===0 &&
    this.state.tel_Errors.length ===0 &&
    this.state.address_Errors.length ===0 &&
    this.state.ville_Errors.length ===0 ){
     
    

    const data = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      company: this.state.company,
      address_1: this.state.address_1,
      city: this.state.city,
      email: this.props.route.params.email,
      country: this.state.country,
      phone: this.state.phone,
      address_2: "",
      lat: this.state.lat,
      lng: this.state.lng,
      customer_note:this.state.customer_note

    };
    console.log(
      "this.props.route.params.item===>",
      this.props.route.params
    );
    const item = {
      id: this.props.route.params.item.id,
      name: this.props.route.params.item.slug,

      quantity: 1,

      price: 1,
    };
    
    console.log("data", data);
    const res = await cerateOrderService(data, item);
    console.log("ressssssss=>", res.status);
    if (res.status === 201) {
      this.props.navigation.navigate("ComfirmedCommande", { data: res.data });
    }



  }
  };

  render() {
    const {
      first_name_Errors,
      tel_Errors,
      address_Errors,
      ville_Errors
    } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <LinearGradient
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          colors={["#4dbeff", "#45a7ee", "#3c8fdc"]}
          style={styles.linearGradient}
        >
          <View
            style={{
              height: 70,
              justifyContent: "space-between",
              padding: 10,
              flexDirection: "row",
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <View
                  style={{ alignContent: "center", justifyContent: "center" }}
                >
                  <Ionicons name={"chevron-left"} size={20} color="white" />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <Text style={{ color: "white", fontSize: 15 }}>
                Votre Commande
              </Text>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            ></View>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "white",
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            }}
          >
            <ScrollView style={{ marginTop: 20 }}>
              <View style={{ flex: 1, marginHorizontal: 25 }}>
                <View style={{ flex: 3 }}>
                  <View style={{ flex: 1, marginTop: 5 }}>
                    <Text>Votre Nom et Prenom</Text>
                    <TextInput
                      placeholder="Votre Nom et Prenom"
                      style={{
                        height: 30,
                        borderWidth: 0.5,
                        borderRadius: 5,
                        marginTop: 5,
                        paddingVertical: 1,
                      }}
                      onChangeText={(e) => {
                        this.setState({
                          first_name: e,
                        });
                      }}
                    ></TextInput>
                  </View>
                  
                {first_name_Errors.map((item, key) => (
                  <Text key={`pwdError-${key}`} style={{fontSize:10,color:"red"}}>
                    {item}
                  </Text>
                ))}
             
                  <View style={{ flex: 1, marginTop: 5 }}>
                    <Text>Votre téléphone</Text>
                    <TextInput
                      placeholder="Votre téléphone"
                      style={{
                        height: 30,
                        borderWidth: 0.5,
                        borderRadius: 5,
                        marginTop: 5,
                        paddingVertical: 1,
                      }}
                      value={this.state.phone}
                      onChangeText={(e) => {
                        console.log(e);
                        this.setState({
                          phone: e,
                        });
                      }}
                    ></TextInput>
                  </View>
                  {tel_Errors.map((item, key) => (
                  <Text key={`telError-${key}`}  style={{fontSize:10,color:"red"}} >
                    {item}
                  </Text>
                ))}
                  <View style={{ flex: 1, marginTop: 5 }}>
                    <Text>Date et heure quand vous souhaitez être contacté</Text>
                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flex: 1, marginRight: 5 }}>
                        <TouchableOpacity
                          onPress={() => this.showDatepicker("date")}
                          style={{
                            height: 30,
                            borderWidth: 0.5,
                            borderRadius: 5,
                            marginTop: 5,
                            paddingVertical: 1,
                            justifyContent: "center",
                          }}
                        >
                          <Text style={{ marginLeft: 5, color: "#bdbdbd" }}>
                            {this.state.dateReservation === null
                              ? "jj/mm/aa"
                              : this.state.dateReservation}
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={{ flex: 1, marginLeft: 5 }}>
                        <TouchableOpacity
                          onPress={() => this.showDatepicker("time")}
                          style={{
                            height: 30,
                            borderWidth: 0.5,
                            borderRadius: 5,
                            marginTop: 5,
                            padding: 1,

                            justifyContent: "center",
                          }}
                        >
                          <Text style={{ marginLeft: 5, color: "#bdbdbd" }}>
                            {this.state.heur === null
                              ? "hh:mm"
                              : this.state.heur}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                  <View style={{ flex: 1, marginTop: 5 }}>
                    <Text>Adresse de l'intervention</Text>
                    <TextInput
                      placeholder="Adresse de l'intervention"
                      style={{
                        height: 30,
                        borderWidth: 0.5,
                        borderRadius: 5,
                        marginTop: 5,
                        paddingVertical: 1,
                      }}
                      onChangeText={(e) => {
                        this.setState({
                          address_1: e,
                        });
                      }}
                    ></TextInput>
                      {address_Errors.map((item, key) => (
                  <Text key={`addressError-${key}`}  style={{fontSize:10,color:"red"}} >
                    {item}
                  </Text>
                ))}
                  </View>
                  <View style={{ flex: 1, marginTop: 5 }}>
                    <Text>Ville</Text>
                    <TextInput
                      placeholder="Ville"
                      style={{
                        height: 35,
                        borderWidth: 0.5,
                        borderRadius: 5,
                        marginTop: 5,
                        paddingVertical: 1,
                      }}
                      onChangeText={(e) => {
                        this.setState({
                          city: e,
                        });
                      }}
                    ></TextInput>
                       {ville_Errors.map((item, key) => (
                  <Text key={`villeError-${key}`}  style={{fontSize:10,color:"red"}} >
                    {item}
                  </Text>
                ))}
                    
                  </View>
                </View>
                <View
                  style={{
                    height: 30,
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <View
                    style={{ height: 0.9, flex: 1, backgroundColor: "#bdbdbd" }}
                  ></View>
                  <View
                    style={{
                      height: 30,
                      width: 20,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Text style={{ color: "#bdbdbd" }}>OU</Text>
                  </View>
                  <View
                    style={{ height: 0.9, flex: 1, backgroundColor: "#bdbdbd" }}
                  ></View>
                </View>
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1, margin: 5, flexDirection: "row" }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          isOpen: true,
                        });
                      }}
                      style={{
                        height: 40,
                        width: 40,
                        backgroundColor: "#c5cae9",
                        borderRadius: 5,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Ionicons name={"map-marker"} size={20} color="#0d47a1" />
                    </TouchableOpacity>
                    <View style={{ margin: 5, justifyContent: "center" }}>
                      <Text style={{ fontSize: 7 }}>
                        Partager votre postion
                      </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <View
                        style={{
                          alignItems: "center",
                          flex: 1,
                          flexDirection: "row",
                        }}
                      >
                        <Text style={{ fontSize: 7 }}>laltidude :</Text>
                        <Text style={{ fontSize: 7 }}>{this.state.lat} </Text>
                      </View>
                      <View
                        style={{
                          alignItems: "center",
                          flex: 1,
                          flexDirection: "row",
                        }}
                      >
                        <Text style={{ fontSize: 7 }}>langitude :</Text>
                        <Text style={{ fontSize: 7 }}>{this.state.lng} </Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ flex: 1 }}>
                    <TouchableOpacity
                      onPress={() => this.Suivant()}
                      style={{
                        height: 35,
                        backgroundColor: "#45a7ee",
                        borderRadius: 5,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text style={{ color: "white" }}> Suivant</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              {this.state.show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={this.state.date}
                  mode={this.state.type}
                  is24Hour={true}
                  display="default"
                  onChange={(e) => this.onChange(e)}
                />
              )}

              <CustomMap
                close={() => {
                  this.setState({
                    isOpen: false,
                  });
                }}
                isOpen={this.state.isOpen}
                data={(e) => {
                  this.getDetaille(e);
                }}
              />
            </ScrollView>
          </View>
        </LinearGradient>
      </View>
    );
  }
}
var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
});
