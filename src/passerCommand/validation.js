import validator from "validator";
validateEmail = (email) => {
  const errors = [];
  const isEmpty = validator.isEmpty(email);
  const isEmail = validator.isEmail(email);

  if (isEmpty) {
    errors.push("Veuillez compléter ce champ");
  }
  if (!isEmpty && !isEmail) {
    errors.push(`Format d'email est invalide`);
  }

  return errors;
};

validatePassword = (password) => {
  const errors = [];
  const isEmpty = validator.isEmpty(password);

  if (isEmpty) {
    errors.push("Veuillez compléter ce champ");
  }

  return errors;
};

validateDate = (date) => {
    const errors = [];
    const isEmpty = validator.isEmpty(date);
  
    if (isEmpty) {
      errors.push("Veuillez compléter ce champ");
    }
  
    return errors;
  };

  
validateNom = (nom, props) => {
  const errors = [];
  const isEmpty = validator.isEmpty(nom);

  if (isEmpty) {
    errors.push("Veuillez compléter ce champ");
  }

  return errors;
};
validatetelephone = (nom) => {
    const errors = [];
    const isEmpty = validator.isMobilePhone(nom);
 
    if (!isEmpty) {
      errors.push("Veuillez compléter ce champ");
    }
  
    return errors;
  };

  validateAddress = (nom) => {
    const errors = [];
    const isEmpty = validator.isEmpty(nom);
  
    if (isEmpty) {
      errors.push("Veuillez compléter ce champ");
    }
  
    return errors;
  };
  validateVille = (nom) => {
    const errors = [];
    const isEmpty = validator.isEmpty(nom);
  
    if (isEmpty) {
      errors.push("Veuillez compléter ce champ");
    }
  
    return errors;
  };
validateConfirmPassword = (password, confirmPassword, props) => {
  const errors = [];
  const isEmpty = validator.isEmpty(confirmPassword);

  if (isEmpty) {
    errors.push("Veuillez compléter ce champ");
  }

  if (!isEmpty && password !== confirmPassword) {
    errors.push("valide vide errone");
  }

  return errors;
};
export default {
  validateEmail,
  validatePassword,
  validateConfirmPassword,
  validateDate,
  validateNom,
  validatetelephone,
  validateAddress,
  validateVille
};
