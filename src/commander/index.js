/* eslint-disable no-console */
import React from 'react';
import { AppRegistry, StyleSheet, Text, TouchableOpacity, View,Image,TextInput ,Linking} from 'react-native';
// eslint-disable-next-line import/no-unresolved
import { RNCamera ,RNFS} from 'react-native-camera';
import { ScrollView } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/FontAwesome';

import LinearGradient from 'react-native-linear-gradient';
import { ImageBackground } from 'react-native';
const flashModeOrder = {
  off: 'on',
  on: 'auto',
  auto: 'torch',
  torch: 'off',
};

const wbOrder = {
  auto: 'sunny',
  sunny: 'cloudy',
  cloudy: 'shadow',
  shadow: 'fluorescent',
  fluorescent: 'incandescent',
  incandescent: 'auto',
};

const landmarkSize = 2;

export default class CameraScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          image:null,
          prandrephoto:false,
          customer_note:""
        };
      }
      componentDidMount(){
        // console.log("itemitemitem=>",this.props.route.params.item)
      }
    render() {
        return (
            <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={{flex:1}}>
            

            <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
            <TouchableOpacity
            style={{height:60,width:70,justifyContent:"center"}}
    activeOpacity={0.8}  onPress={()=> this.props.navigation.goBack()}>
           <View    style={{alignContent:"center",justifyContent:"center"}}>
          
            <Ionicons name={"chevron-left"} size={20}  color="white"/>
            </View>
            </TouchableOpacity>
            <View style={{alignContent:"center",justifyContent:"center",flex:1}}>
        <Text style={{color:"white",fontSize:20,fontWeight: 'bold'}} numberOfLines={1} ellipsizeMode='tail'> {this.props.route.params.last}/{this.props.route.params.item.name}</Text>
               </View>
            <View style={{height:60,width:70}}></View>
            
            
            </View>


         <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
        <View style={{flex:1}}>
         <ScrollView style={{flex:1}}>
              {/* {
              this.state.image === null &&
               this.state.prandrephoto ?

             
           <View style={{flex:1,marginTop:50,height:150}}>
           <RNCamera
             ref={ref => {
                this.camera = ref;
              }}
              style={styles.preview}
              type={RNCamera.Constants.Type.back}
              flashMode={RNCamera.Constants.FlashMode.on}
              androidCameraPermissionOptions={{
                title: 'Permission to use camera',
                message: 'We need your permission to use your camera',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              androidRecordAudioPermissionOptions={{
                title: 'Permission to use audio recording',
                message: 'We need your permission to use your audio',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              onGoogleVisionBarcodesDetected={({ barcodes }) => {
                console.log(barcodes);
              }}
            />
            {
              this.state.prandrephoto ===true  &&
              <View style={{ height:70, flexDirection: 'row', justifyContent: 'center' }}>
    
              <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                <Text style={{ fontSize: 14,color:"white" }}> Prendre Image </Text>
              </TouchableOpacity>
            </View>
            }
            
           </View>
           
           :null
           }
           <View style={{flex:1,marginTop:50}}>
           <View style={{flex:1}}> 
           <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
         {this.state.image !=null & this.state.prandrephoto ? 

         
           <Image
             resizeMode="stretch"
           style={{height:300,width:300,borderRadius:10}}
           source={{uri:this.state.image}}
           ></Image>:
           <View style={{height:100,width:300}}
           >
             {
               this.state.prandrephoto ===false &&
               <View style={{alignContent:"center",alignItems:"center"}}>
                 <Text style={{fontSize:11,margin:10,fontWeight: 'bold'}}>Vous pouvez nous envoyer une photo de votre besoin</Text>
                 <TouchableOpacity
               activeOpacity={0.8}  onPress={()=> {
                this.setState({
                 prandrephoto:true
                })
                 }} style={{backgroundColor:"#283593",height:40,borderRadius:5,alignItems:"center",justifyContent:"center",width:"80%"}}>
                          <Text style={{color:"white",fontSize:15}}>Photo</Text>
           
                        </TouchableOpacity>
               </View>
               
             }
          
           </View>
          } 
          
           </View>
           */}
           <View>
             <View>
               <View style={{marginTop:30,alignItems:"center"}}>
               <Text style={{fontSize:15,margin:10,color:"black"}}>Envoyez-nous une photo par whatsapp</Text>
               </View>
               <View style={{height:100,alignItems:"center",marginBottom:20}}>
               <TouchableOpacity 
                 onPress={()=>{
                    Linking.openURL('whatsapp://send?phone=+212666517022')
                }}
               >

              

   
             <Image
             resizeMode="stretch"
           style={{height:80,width:80,borderRadius:10}}
           source={require('../assets/watsap.png')}
           ></Image>
            </TouchableOpacity>
           </View>
             </View>
             <View>
           {/* <View style={{flexDirection:"row",height:40}}>
               <View style={{flex:3,alignItems:"center",justifyContent:"center"}}>
               <Text style={{fontWeight: 'bold'}}>
               Le tarif sera défini avec notre agent sur place
               </Text>
               </View>
               
           </View> */}
           <View style={{flex:1,margin:10}}>
            <TextInput
            placeholder="Merci nous laisser un commentaire"
            onChangeText={(e)=>{
              this.setState({
                customer_note:e
              })
            }}
            style={{height:"55%",borderColor:"#9e9e9e",borderWidth:1,borderRadius:10,white:"100%"}}
    multiline={true}
    numberOfLines={4}
    />
    <View style={{alignItems:"center",justifyContent:"center",marginTop:10}}>
    <Text style={{fontSize:15,margin:5,color:"#1565c0"}}>Le tarif sera défini avec </Text>
    <Text style={{fontSize:15,margin:5,color:"#1565c0"}}>notre agent sur place</Text>
    </View>
   
            </View>
           </View>
           </View>
           </ScrollView>
           <TouchableOpacity
    activeOpacity={0.8}  onPress={()=> {
     
      this.props.navigation.navigate("VotreCommande",{"item":this.props.route.params.item,"customer_note":this.state.customer_note})}}>
    
    <View style={{height:50,backgroundColor:"#283593",margin:20,alignContent:"center",alignItems:"center",justifyContent:"center",borderRadius:5}}>
<Text style={{color:"white",fontSize:20}}>Demande d’intervention</Text>
    </View>
    </TouchableOpacity>
           </View>
         
           </View>
          </LinearGradient>
        );
      }
   getBase64= async(imageUri)=> {
        
       
        return `data:image/jpeg;base64,${imageUri}`;
      }
    
      takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
          console.log('base64: ', data.base64);
  //show image to user now
  const Imagebase64 = await this.getBase64(data.base64);
 
 this.setState({
     image:Imagebase64
 })
        }
      };
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
      },
      preview: {
        flex: 1,
     
        backgroundColor:"red",
        justifyContent: 'flex-end',
        alignItems: 'center',
      },
      capture: {
          width:"80%",
          alignItems:"center",
        backgroundColor:"#283593",
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
      },
    });
    