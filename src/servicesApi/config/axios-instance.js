import axios from 'axios';


export const X_API_VERSION = 'MOBILE-TITULAIRE';
export const Content_Type = 'application/json';
export const ACCEPT = 'application/json';
export const modDev = 'prod';

 export const BASE_URL = 'https://soviny.happyagency.biz/';  //dev
//export const BASE_URL = 'https://soviny.ma/';  // prod

const baseConfig = {
  baseURL: BASE_URL,
  timeout: 30000,
};


export const authInstance = axios.create(baseConfig);


export  const  consumer_key="ck_3a312eb00f57a13939f5f7203bb52fb2f5261510";
export  const    consumer_secret="cs_1769e0739d532f4a5e69a8773c38ec6e38db9af7";


authInstance.interceptors.request.use(
  async config => {
    config.headers.common['ACCEPT'] = ACCEPT;
    config.headers.common['Content_Type'] = Content_Type;
    return config;
  },
  error => error,
);


export default {
  authInstance,
  
  consumer_key,
  consumer_secret

};
