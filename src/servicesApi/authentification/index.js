import {authInstance} from '../config/axios-instance';
import * as Sentry from '@sentry/react-native';
import axios from 'axios';
export async function LoginService(username , password) {
  try {
   
    const response = await authInstance.post("wp-json/jwt-auth/v1/token",{
   
        username,
        password
       
      
    });
    console.log("resss==>",response)
    return response;
  } catch (e) {
    console.log("ress  errreurs==>",e)
    return e;
  }
}
export async function Register(username ,email, password) {
  console.log("usernameusername>",username)
  console.log("emailllllllll>",email)
  console.log("password",password)
  try {
 
    const responseGetNoce = await authInstance.get("api/get_nonce/?controller=user&method=register",{
      
    });
    console.log("responseGetNoce===>",JSON.stringify(responseGetNoce.data))
    if(responseGetNoce.status===200){
 const response = await authInstance.get("api/user/register/",{
      params: {
        username,
        user_pass: password,
        nom: username,
        email,
        "nonce": responseGetNoce.data.nonce,
        "nickname" :email

       
      },
    });
    console.log("resss==>",response)
    return response;
    }

    console.log("responseGetNoce===>",responseGetNoce)
    return 

   
  } catch (e) {
    Sentry.captureException(e);
    console.log("ress  errreurs==>",e)
    return e.reponse;
  }
}


export async function RestPasswordService(email) {
  try {
   
    const response = await authInstance.get("api/user/retrieve_password/",{
      params: {
        user_login:email

       
      },
    });
    console.log("resss==>",response)
    return response;
  } catch (e) {
    console.log("ress  errreurs==>",e)
    return e.reponse;
  }
}


export async function changePassword(password ,id, token) {
  console.log("changePassword========>",password ,id, token)
  try {
   
    const response = await axios.post("https://soviny.happyagency.biz/wp-json/wp/v2/users/"+id,{   
        password :password  
    },{
      headers: {
        
          'Content-Type': 'application/json',
          'Authorization':'Bearer '+token,
      }
    });
    console.log("resss==>",response)
    return response;
  } catch (e) {
    console.log("ress  errreurs==>",e)
    return e;
  }
}