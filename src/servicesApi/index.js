//import WooCommerceRestApi from"@woocommerce/woocommerce-rest-api";
 
import WooCommerceAPI from 'react-native-woocommerce-api';
 
var api = new WooCommerceAPI({
    url: "https://soviny.happyagency.biz/", // Your store URL
  ssl: true,
  consumerKey: "ck_8f45283fbaeba50e03151f3b43b886e3ccb9c094", // Your consumer secret
  consumerSecret: "cs_ea2a5e9781c527551fc0a6cb0b8ea190ddeb1d72", // Your consumer secret
  wpAPI: true, // Enable the WP REST API integration
  version: 'wc/v2', // WooCommerce WP REST API version
  queryStringAuth: true
});
// const api = new WooCommerceRestApi({
//   url: "https://soviny.happyagency.biz/",
//   consumerKey: "ck_8f45283fbaeba50e03151f3b43b886e3ccb9c094",
//   consumerSecret: "cs_ea2a5e9781c527551fc0a6cb0b8ea190ddeb1d72",
//   version: "wc/v3"
// });
 
// List products
export async function getProductTags() {
  const data = {
    featured: true,
    "downloads":"downloads",
    "params": {
      "per_page":100,
      "parent":0
    }
  };
 
  // products
  // orders
  //posts
  //products/tags
  try {
    const response = await api.get("products/categories",data)
  
console.log("ressss===>",response)
    return response;
  } catch (error) {
    console.log("resssserrorerror===>",error)
    return error.response;
  }

 
}
export async function getImageProductTags(id) {
  const data = {
    featured: true,
    "downloads":"downloads"
  };
 
  // products
  // orders
  //posts
  //products/tags
  try {
    const response = await api.get( `products/categories/${id}`,data)
  
console.log(response)
    return response;
  } catch (error) {
    return error.response;
  }

 
}

// Create a product
export async function cerateProduct() {
api.post("products", {
  name: "Premium Quality", // See more in https://woocommerce.github.io/woocommerce-rest-api-docs/#product-properties
  type: "simple",
  regular_price: "21.99",
})
  .then((response) => {
    // Successful request
    console.log("Response Status:", response.status);
    console.log("Response Headers:", response.headers);
    console.log("Response Data:", response.data);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log("Response Status:", error.response.status);
    console.log("Response Headers:", error.response.headers);
    console.log("Response Data:", error.response.data);
  })
  .finally(() => {
    // Always executed.
  });
}

// Edit a product
export async function editproduc() {
api.put("products/1", {
  sale_price: "11.99", // See more in https://woocommerce.github.io/woocommerce-rest-api-docs/#product-properties
})
  .then((response) => {
    // Successful request
    console.log("Response Status:", response.status);
    console.log("Response Headers:", response.headers);
    console.log("Response Data:", response.data);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log("Response Status:", error.response.status);
    console.log("Response Headers:", error.response.headers);
    console.log("Response Data:", error.response.data);
  })
  .finally(() => {
    // Always executed.
  });
}

export async function deleteProduct() {
// Delete a product
api.delete("products/1", {
  force: true, // Forces to delete instead of move to the Trash
})
  .then((response) => {
    // Successful request
    console.log("Response Status:", response.status);
    console.log("Response Headers:", response.headers);
    console.log("Response Data:", response.data);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log("Response Status:", error.response.status);
    console.log("Response Headers:", error.response.headers);
    console.log("Response Data:", error.response.data);
  })
  .finally(() => {
    // Always executed.
  });
}