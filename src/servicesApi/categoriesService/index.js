import {authInstance, consumer_key,
  consumer_secret
} from '../config/axios-instance';
import * as Sentry from '@sentry/react-native';

export async function getCategoriesService(page = 0, size = 10) {
  try {
    const response = await authInstance.get("wp-json/wc/v3/products/categories", {
      params: {
        "consumer_key":consumer_key,
    "consumer_secret":consumer_secret,
    "per_page":100,
    "parent":0
       
      },
    });
    console.log("resss==>",response)
    return response;
  } catch (e) {
    Sentry.captureException(e);
    console.log("ress  errreurs==>",e)
    return e.reponse;
  }
}

