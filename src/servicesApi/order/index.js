import {authInstance, consumer_key,
    consumer_secret} from '../config/axios-instance';
    import * as Sentry from '@sentry/react-native';

export async function cerateOrderService(data,item) {
  console.log("cerateOrderService(data,item)==>",item)
  try {
    const response = await authInstance.post("wp-json/wc/v3/orders", {"payment_method": "bacs",
    "payment_method_title": "Paiement à la livraison",
    "status":"processing",
    "number":4,
    "billing": data,
    "customer_note":data.customer_note,
    "lat":data.lat,
    "lng":data.lng,
    "meta_data": [
    
      {
          "key": "billing_lat",
          "value": data.lat
      },
      {
          "key": "billing_long",
          "value": data.lng,
      },
    
  ],

    "line_items": [
       {
        "product_id": item.id,
        "name": item.name,
        "quantity": 1,
        
        "price": 1,
       }
      ],
      "shipping_lines": []
  },
  {
    params: {
      "consumer_key":consumer_key,
  "consumer_secret": consumer_secret
    },
  }

    );
    console.log("resss==>",response)
    return response;
  } catch (e) {
    Sentry.captureException(e);
    console.log("ress  errreurs==>",e)
    return e.reponse;
  }
}


export async function getOrderService(data,item) {
  console.log("cerateOrderService(data,item)==>",item)
  try {
    const response = await authInstance.get("wp-json/wc/v3/orders",{
      params: {
        "consumer_key":consumer_key,
    "consumer_secret":consumer_secret
      }
    });
    console.log("resss==aaaaaaa>",response.data[0].status)
    return response;
  } catch (e) {
    Sentry.captureException(e);
    console.log("ress  errreurs==>",e)
    return e.reponse;
  }
}