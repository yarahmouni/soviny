import React, { Component } from 'react'
import { Text, View ,StyleSheet,FlatList, ImageBackground,Dimensions, TouchableHighlight,Image,TouchableOpacity} from 'react-native'
import Ionicons from 'react-native-vector-icons/FontAwesome';
import {getProductTags, getImageProductTags} from '../servicesApi/index'
import {getCategoriesService} from '../servicesApi/categoriesService/index'
import LinearGradient from 'react-native-linear-gradient';
import { SafeAreaView } from 'react-native-safe-area-context';
import Footer from '../components/footer';
import ItemHome from './item_home';

  const {width,height} = Dimensions.get('window');
 const listperiter=["Plomberie","Électricité","Jardinage","Maçonnerie","Peinture","Ménages","Électroménager","Sols","Serrurerie","Petits Travaux"]
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categorise:[],
      isFetching:false
    };
  }
 
 getP= async()=>{
   this.setState({isFetching:true})
   const res= await getCategoriesService()
   if(res){
   if(res.data){
   const newList = res.data.filter((item) => item.name !== "Autre");
   console.log("new list===>",newList)
   if(res.status ===200){
    await  this.changeoRDER(res.data)
   
   
    /// console.log(res.data)
     
   }}
   }

  }

  changeoRDER= async(listCat=[])=>{
let arr=[]
    for(let i=0;i<listperiter.length;i++){
    
     
      
 
   arr.push(listCat.findIndex(x => x.name ===listperiter[i]));

  
  }
  let nl=[]
  let ob={}
  console.log("arr.length",10)
  for(let k=0;k<arr.length;k++){
 ob=arr[k];
 console.log("::::>",listCat[ob])
 console.log("k==>",k)
nl.push(listCat[ob])
  }
  console.log()
  for(let j=arr.length;j<listCat.length-1;j++){
    nl.push([listCat[j]])

  }
  console.log("sure==>",nl)

 

 
  this.centerIcon(nl)
  }
  centerIcon=(list=[])=>{
    let nls=[];
    let subl=[]
    let j=0;
    for(let i=0;i<list.length;i++){
       subl.push(list[i])
      j=j+1
      if(j===3 ||i==list.length-1 ){
        nls.push(subl);
        subl=[]
        j=0
      }
     


    }
    this.setState({
      categorise:nls,
     isFetching:false
     })
    console.log('lll====>',)

  }
  componentDidMount(){
  
this.getP()
  }
  renderItem = ({item}) =>  (
   
    <TouchableOpacity   onPress={()=>{this.props.navigation.navigate("Settings",{item})}}>
    <View style={{ width: ((width-55)/3), margin:7,alignContent:"center",justifyContent:"center",alignItems:"center"}} key={item}>
       <View>
        <ImageBackground
      resizeMode="stretch"
style={{
margin:2,
width: ((width-55)/3),
height: ((width-55)/3),
alignItems: 'center',
justifyContent: 'center',

}}
        source={{
            uri: item.image && item.image.src ? item.image.src:"",
          }}
        />
        </View>
        <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}}>
         <Text style={{textAlign:"center",fontSize:11,color:"black",   fontWeight: "bold"}}>
            {item.name}
        </Text>
        </View>
    </View>
    
  </TouchableOpacity>
   
  )



    render() {
        return (
        
            <View style={{flex:1}}>
            <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={styles.linearGradient}>
            <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
              
           <View style={{alignItems:"flex-start",justifyContent:"center",alignContent:"center",height:60,width:70}} >
           <TouchableOpacity  style={{height:60,width:70,justifyContent:"center"}}  onPress={()=>{this.props.navigation.toggleDrawer()}}>
           
            <Ionicons name={"bars"} size={30}  color="white"/>
            </TouchableOpacity>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center",flex:1}} >
              <Image  style={{height:35,width:100}}    resizeMode="stretch"  source={require('../assets/logoblancMobile.png')}></Image>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center",width:70}} >
            {/* <Ionicons name={"search"} size={30}  color="white"/> */}
            </View>
            </View>
            <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
            <View style={{marginHorizontal:30}}>
            <View style={{justifyContent:"center",height:50}}>
                <Text style={{fontSize:15,color:"black", fontWeight: 'bold'}}>
                Quel est votre besoin ?
                </Text>
                </View>
            </View>
            <View style={{flex:1,marginHorizontal:5}}>
            <FlatList
          //  onRefresh={() => this.getP()}
          //  refreshing={this.state.isFetching}
        data={this.state.categorise}
        numColumns={1}
        renderItem={({ item, index }) => (
          <ItemHome data={item} navigation={this.props.navigation}></ItemHome>
        )}
          
        
        keyExtractor={item => item.id}
      />
          
            </View>
             </View>
             
           </LinearGradient>
          <Footer></Footer>
           </View>
      
        
        )
    }
}
var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
   
     
    }
   
  });
