import React, { Component } from 'react'
import { Text, View,Dimensions,TouchableOpacity ,ImageBackground} from 'react-native'
const {width,height} = Dimensions.get('window');
export default class ItemHome extends Component {
    render() {

        return (
            <View style={{flex:1,alignItems:"center",flexDirection:"row",alignItems:"center"}}>
                {
                    this.props.data?.length===1 ?
                    <View style={{flex:1,alignItems:"center",flexDirection:"row",alignItems:"center"}}>
                      
                      <View style={{flex:1}}></View>
                      <View style={{flex:1}}>

                      <TouchableOpacity  
                     onPress={()=>{this.props.navigation.navigate("Settings",{item:this.props.data[0]})}}
                     >
        <View style={{ width: ((width-55)/3), margin:7,alignContent:"center",justifyContent:"center",alignItems:"center"}} key={this.props.data[0]}>
           <View>
            <ImageBackground
          resizeMode="stretch"
    style={{
    margin:2,
    width: ((width-55)/3),
    height: ((width-55)/3),
    alignItems: 'center',
    justifyContent: 'center',
    
    }}
            source={{
                uri: this.props.data[0]?.image && this.props.data[0]?.image.src ? this.props.data[0]?.image.src:"",
              }}
            />
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}}>
             <Text style={{textAlign:"center",fontSize:11,color:"black",   fontWeight: "bold"}}>
                {this.props.data[0]?.name}
            </Text>
            </View>
        </View>
        
      </TouchableOpacity>
                      </View>
                      <View style={{flex:1}}></View>
                    </View> :

                    <View style={{flex:1,alignItems:"center",flexDirection:"row",alignItems:"center"}}>
                   <View style={{flex:1}}>
                     
                    <TouchableOpacity  
                     onPress={()=>{
                         this.props.navigation.navigate("Settings",{item:this.props.data[0]})
                      
                        }}
                     >
        <View style={{ width: ((width-55)/3), margin:7,alignContent:"center",justifyContent:"center",alignItems:"center"}} key={this.props.data[0]}>
           <View>
            <ImageBackground
          resizeMode="stretch"
    style={{
    margin:2,
    width: ((width-55)/3),
    height: ((width-55)/3),
    alignItems: 'center',
    justifyContent: 'center',
    
    }}
            source={{
                uri: this.props.data[0]?.image && this.props.data[0]?.image.src ? this.props.data[0]?.image.src:"",
              }}
            />
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}}>
             <Text style={{textAlign:"center",fontSize:11,color:"black",   fontWeight: "bold"}}>
                {this.props.data[0]?.name}
            </Text>
            </View>
        </View>
        
      </TouchableOpacity>
                   
                    </View>
                    <View style={{flex:1}}>
                      {this.props.data?.length>1 ?
                    <TouchableOpacity  
                     onPress={()=>{this.props.navigation.navigate("Settings",{item:this.props.data[1]})}}
                     >
        <View style={{ width: ((width-55)/3), margin:7,alignContent:"center",justifyContent:"center",alignItems:"center"}} key={this.props.data[1]}>
           <View>
            <ImageBackground
          resizeMode="stretch"
    style={{
    margin:2,
    width: ((width-55)/3),
    height: ((width-55)/3),
    alignItems: 'center',
    justifyContent: 'center',
    
    }}
            source={{
                uri: this.props.data[1]?.image && this.props.data[1]?.image.src ? this.props.data[1]?.image.src:"",
              }}
            />
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}}>
             <Text style={{textAlign:"center",fontSize:11,color:"black",   fontWeight: "bold"}}>
                {this.props.data[1]?.name}
            </Text>
            </View>
        </View>
        
      </TouchableOpacity> : null}
                    </View>
                    <View style={{flex:1}}>
                      {this.props.data?.length>2 ?
                    <TouchableOpacity  
                     onPress={()=>{this.props.navigation.navigate("Settings",{item:this.props.data[2]})}}
                     >
        <View style={{ width: ((width-55)/3), margin:7,alignContent:"center",justifyContent:"center",alignItems:"center"}} key={this.props.data[2]}>
           <View>
            <ImageBackground
          resizeMode="stretch"
    style={{
    margin:2,
    width: ((width-55)/3),
    height: ((width-55)/3),
    alignItems: 'center',
    justifyContent: 'center',
    
    }}
            source={{
                uri: this.props.data[2]?.image && this.props.data[2]?.image.src ? this.props.data[2]?.image.src:"",
              }}
            />
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}}>
             <Text style={{textAlign:"center",fontSize:11,color:"black",   fontWeight: "bold"}}>
                {this.props.data[2]?.name}
            </Text>
            </View>
        </View>
        
      </TouchableOpacity>  :null}
                    </View>
                    </View>
                
                }
              
               
            </View>
        )
    }
}
