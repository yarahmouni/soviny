
import React, { Component } from 'react'
import { Text, View ,Image,TouchableOpacity,StyleSheet,TextInput} from 'react-native'
import Ionicons from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';


export default class ComfirmedCommande extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data:{}
        };
      }
      componentDidMount(){
          this.setState({
              data:this.props.route.params.data
          })
      }
    render() {
        return (
            <View style={{flex:1}}>
            <LinearGradient   start={{  x: 0, y: 1 }} end={{ x: 1, y: 1 }} colors={['#4dbeff','#45a7ee', '#3c8fdc']} style={styles.linearGradient}>
            <View style={{height:70,justifyContent:"space-between",padding:10,flexDirection:"row"}}>
              
           <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}} >
           <TouchableOpacity 
           style={{height:60,width:70,justifyContent:"center"}}
           onPress={()=> this.props.navigation.goBack()}>
           <View style={{alignContent:"center",justifyContent:"center"}}>
            <Ionicons name={"chevron-left"} size={20}  color="white"/>
            </View>
            </TouchableOpacity>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center"}} >
              <Text style={{color:"white",fontSize:20,fontWeight: 'bold'}} numberOfLines={1} ellipsizeMode='tail'>Votre commande</Text>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",alignContent:"center",height:60,width:70}} >
             
            </View>
            </View>
            <View style={{flex:1,backgroundColor:"white", borderTopLeftRadius:30,borderTopRightRadius:30}}>
            <View style={{marginHorizontal:30,alignItems:"center",justifyContent:"center",marginTop:10}}>
           <Text style={{color:'black',fontSize:15}}> Commande N°MA-{this.state.data.id} </Text>
            </View>
            <View style={{alignItems:'center',justifyContent:"center",margin:30}}>
                <Text style={{fontSize:20,textAlign:"center",color:"#00227b"}}>
                    MERCI POUR VOTRE CONFIANCE !
                </Text>
            </View>
            <ScrollView style={{flex:1,marginHorizontal:20}}>
                <View style={{alignItems:"center",justifyContent:"center",margin:20}}>
            <Text>
                Nous avons bien pris en compte votre
            </Text>
            <Text>
               demande d'intervention
            </Text>
            </View>
            <View style={{alignItems:"center",justifyContent:"center",margin:5}}>
            <Text>
               Un de nos experts prendra contact avec
            </Text>
            <Text>
            vous sous peu pour confirmer
            </Text>
            <Text>
            son passage
            </Text>
            </View>

            <View style={{alignItems:"center",justifyContent:"center",margin:20}}>
            <Text>
                Nous recevrez également un email
            </Text>
            <Text>
              avec les détails de votre commande
            </Text>
            </View>
            </ScrollView>
            <TouchableOpacity onPress={()=>   this.props.navigation.navigate("Home")} style={{height:35,borderWidth:1,borderColor:"#00227b",margin:15,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
             <Text style={{color:"#00227b"}}>Accueil</Text>
               </TouchableOpacity>
               <TouchableOpacity onPress={()=> this.props.navigation.navigate("Contacter") } style={{height:35,backgroundColor:"#00227b",margin:15,borderRadius:5,alignItems:"center",justifyContent:"center"}}>
             <Text style={{color:"white"}}>Nous contacter</Text>
               </TouchableOpacity>
             </View>
           </LinearGradient>
           </View>
        )
    }
}

var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
   
     
    }
   
  });
