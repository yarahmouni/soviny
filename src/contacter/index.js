import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Dimensions,
  Linking,
  ImageBackground
} from "react-native";
import Ionicons from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import { ScrollView } from "react-native-gesture-handler";
import ItemContact from "../components/item_contact";
import { SocialIcon, Button } from "react-native-elements";
import Share from 'react-native-share';
const { width, height } = Dimensions.get("window");

const shareEmailImage = async () => {
  const shareOptions = {
    title: 'soviny',
    failOnCancel: false,
    message: 'SOVINY est une application mobile spécialisée dans les prestations de réparation, de dépannage et de bricolage à domicile ou au bureau. Le but de cette solution est de fournir aux clients particuliers ou entreprises des prestations de qualité permettant de répondre à leurs attentes en matière de réparation, bricolage et service en respectant une qualité de prestation et un tarif honnête et transparent     https://soviny.ma/',
  };

  try {
    const ShareResponse = await Share.open(shareOptions);
    setResult(JSON.stringify(ShareResponse, null, 2));
  } catch (error) {
    console.log('Error =>', error);
    setResult('error: '.concat(getErrorString(error)));
  }
};

export default class Contacter extends Component {
  
  render() {
    return (
      <View style={{ flex: 1 }}>
        <LinearGradient
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          colors={["#4dbeff", "#45a7ee", "#3c8fdc"]}
          style={styles.linearGradient}
        >
          <View
            style={{
              height: 70,
              justifyContent: "space-between",
              padding: 10,
              flexDirection: "row",
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <TouchableOpacity
                style={{ height: 60, width: 70, justifyContent: "center" }}
                onPress={() => this.props.navigation.goBack()}
              >
                <View
                  style={{ alignContent: "center", justifyContent: "center" }}
                >
                  <Ionicons name={"chevron-left"} size={20} color="white" />
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <Text
                style={{ color: "white", fontSize: 20, fontWeight: "bold" }}
                numberOfLines={1}
                ellipsizeMode="tail"
              >
                Nous Contacter
              </Text>
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
                height: 60,
                width: 70,
              }}
            ></View>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "white",
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
            }}
          >
           
              <View style={{ marginTop: 30, marginHorizontal: 20,flex:1, height:"100%" }}>
              <View style={{flex:1,}}>
                <View style={{ alignContent: "center", alignItems: "center" }}>
                 
                </View>
                <ItemContact
                  title="Service commmercial"
                  email="commercial@soviny.ma"
                  phone="08 08 579 261"
                  icon="comments"
                ></ItemContact>
                <ItemContact
                  title="Service technique"
                  email="technique@soviny.ma"
                  phone="08 08 579 261"
                  icon="headset"
                ></ItemContact>
                <ItemContact
                  title="Service aprés vente"
                  email="sov@soviny.ma"
                  phone="08 08 579 261"
                  icon="user"
                ></ItemContact>
                </View>
                <View style={{flex:1,justifyContent:"center"}}>
                  <View style={{flex:1,justifyContent:"center"}}>
                <View
                  style={{ height: 0.8, backgroundColor: "#aeaeae" }}
                ></View>
                <View style={{ flex: 1, marginTop: 20 }}>
                  <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                  <View
                    style={{ alignItems: "center", justifyContent: "center" }}
                  >
                    <Text style={{ color: "black" }}>Nos Réseaux Sociaux</Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      alignContent: "center",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <SocialIcon
                    onPress={()=>Linking.openURL('https://www.facebook.com/Soviny-105574098212243')}
                    
                    type="facebook" />
                   
                    <ImageBackground
                    resizemode={'stretch'}
                    source={require('../../src/assets/instagramcircleicon.png')} style={{height:50,width:51,backgroundColor:'white',resizemode:'stretch'
                  
                  }}
                    >
                      
<TouchableOpacity style={{flex:1}}
 onPress={()=>Linking.openURL('https://www.instagram.com/sovin_y/')}
>

</TouchableOpacity>
                    </ImageBackground>
                    


                  

                    <SocialIcon type="youtube"
                       onPress={()=>Linking.openURL('https://www.youtube.com/channel/UCcgMQUMPh7xIkwDf_X34UTw')}
                    />
                    <SocialIcon type="linkedin"
                       onPress={()=>Linking.openURL('https://www.linkedin.com/company/soviny-by-izybricool')}
                    />
                  </View>
                  </View>
                  <View style={{ margin: 10 }}>
                    <View style={{ marginBottom: 5 }}>
                      <Button title="Partager" type="outline" color="red"  onPress={shareEmailImage}/>
                    </View>
                    <View>
                      <Button
                        title="Accueil"
                        onPress={() => {
                          this.props.navigation.navigate("Home");
                        }}
                      />
                    </View>
                  </View>
                  </View>
                </View>
              </View>
              </View>
           
          </View>
        </LinearGradient>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
});
