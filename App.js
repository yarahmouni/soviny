
import React, { Component } from 'react'
import { View, Text,StatusBar } from 'react-native'
import PropTypes from 'prop-types'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/FontAwesome';
import Home from './src/home';
import SplashScreen from 'react-native-splash-screen'
import Categori from './src/categori';
import SubCategori from './src/subcategori/index'
import { SafeAreaView } from 'react-native-safe-area-context';
import Autre from './src/autre';
import { GlobalStack } from './src/navigation/GloabalNavigator';
import { TabNavigator } from './src/navigation/TabNavigator';
import {DrawerStack} from './src/navigation/MainNavigator'
import SiginUp from './src/signup';
import * as moment from 'moment';
import 'moment/locale/pt-br';

import * as Sentry from '@sentry/react-native';

Sentry.init({ 
  dsn: 'https://ec30076c1b3f44478d296c930829322c@o476195.ingest.sentry.io/5653129', 
  environment: "dev",
  enableNative: true,
});

const Tab = createBottomTabNavigator();
moment.locale('fr');
export default class App extends Component {
 
  componentDidMount(){
    setTimeout(() => {
      SplashScreen.hide();
    }, 1500);
    
   
  }
 

  render() {
    return (
      <SafeAreaView style={{flex:1,backgroundColor:"#4dbeff"}}>
        <StatusBar backgroundColor="#4dbeff"></StatusBar>
      <NavigationContainer>
        <DrawerStack/>
     
     

    

    
    </NavigationContainer>
    </SafeAreaView>
    )
  }
}







